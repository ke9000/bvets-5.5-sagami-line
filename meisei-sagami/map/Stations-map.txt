﻿BveTs Map 2.01
#Bve trainsim 5.5 準拠
	Station.Load('stations.txt');
//橋本駅
0;
//自
	Repeater['FormL0'].Begin0(0,1,25,25,'FormL0');
	Repeater['FormCL0'].Begin0(0, 1, 25,25,'FormCL0');
	Repeater['RoofL1'].Begin0(0, 1, 25,25,'RoofL1');
	Repeater['RoofCL1'].Begin0(0, 1, 25,25,'RoofCL1');
//他
	Repeater['FormR0'].Begin0('101',1,25,25,'FormR0');
	Repeater['FormCR0'].Begin0('101',1,25,25,'FormCR0');
	Repeater['RoofR1'].Begin0('101',1,25,25,'RoofR1');
	Repeater['RoofCR1'].Begin0('101',1,25,25,'RoofCR1');
	
180;
	Station['sta0'].Put(-1,-5,5);
	Structure['Stop'].Put('0',2,0,0,0,0,0,0,5);

200;
//自
	Repeater['FormL0'].End();
	Repeater['FormCL0'].End();
	Repeater['RoofL1'].End();
	Repeater['RoofCL1'].End();
//他
	Repeater['FormR0'].End();
	Repeater['FormCR0'].End();
	Repeater['RoofR1'].End();
	Repeater['RoofCR1'].End();

//南橋本
2090;
//自
	Repeater['FormR0'].Begin0('0',1,25,25,'FormR0');
	Repeater['FormCR0'].Begin0('0',1,25,25,'FormCR0');
	Repeater['RoofR1'].Begin0('0',1,25,25,'RoofR1');
	Repeater['RoofCR1'].Begin0('0',1,25,25,'RoofCR1');
//他
	Repeater['FormL0'].Begin0('101',1,25,25,'FormL0');
	Repeater['FormCL0'].Begin0('101', 1, 25,25,'FormCL0');
	Repeater['RoofL1'].Begin0('101', 1, 25,25,'RoofL1');
	Repeater['RoofCL1'].Begin0('101', 1, 25,25,'RoofCL1');

2180;
	Station['sta1'].Put(1,-5,5);
	Structure['Stop'].Put('0',-2,0,0,0,0,0,0,5);

2190;
//自
	Repeater['FormR0'].End();
	Repeater['FormCR0'].End();
	Repeater['RoofR1'].End();
	Repeater['RoofCR1'].End();
//他
	Repeater['FormL0'].End();
	Repeater['FormCL0'].End();
	Repeater['RoofL1'].End();
	Repeater['RoofCL1'].End();

//上溝
4990;
//自のみ
	Repeater['FormR0'].Begin0('0',1,25,25,'FormR0');
	Repeater['FormCR0'].Begin0('0',1,25,25,'FormCR0');
	Repeater['RoofR1'].Begin0('0',1,25,25,'RoofR1');
	Repeater['RoofCR1'].Begin0('0',1,25,25,'RoofCR1');

5080;
	Station['sta2'].Put(1,-5,5);
	Structure['Stop'].Put('0',-2,0,0,0,0,0,0,5);
	
5090;
//自のみ
	Repeater['FormR0'].End();
	Repeater['FormCR0'].End();
	Repeater['RoofR1'].End();
	Repeater['RoofCR1'].End();

//番田
6490;
//自
	Repeater['FormR0'].Begin0('0',1,25,25,'FormR0');
	Repeater['FormCR0'].Begin0('0',1,25,25,'FormCR0');
	Repeater['RoofR1'].Begin0('0',1,25,25,'RoofR1');
	Repeater['RoofCR1'].Begin0('0',1,25,25,'RoofCR1');
//他
	Repeater['FormL0'].Begin0('101',1,25,25,'FormL0');
	Repeater['FormCL0'].Begin0('101', 1, 25,25,'FormCL0');
	Repeater['RoofL1'].Begin0('101', 1, 25,25,'RoofL1');
	Repeater['RoofCL1'].Begin0('101', 1, 25,25,'RoofCL1');
6510;	
//他
	
	Repeater['FormCL0'].End();
	Repeater['RoofL1'].End();
	Repeater['RoofCL1'].End();
6580;
	Station['sta3'].Put(1,-5,5);
	Structure['Stop'].Put('0',-2,0,0,0,0,0,0,5);


6590;
//自
	Repeater['FormR0'].End();
	Repeater['FormCR0'].End();
	Repeater['RoofR1'].End();
	Repeater['RoofCR1'].End();
//他
	Repeater['FormL0'].End();
	Repeater['RoofR1'].End();

//原当麻
8460;
//自
	Repeater['FormR0'].Begin0('0',1,25,25,'FormR0');
	Repeater['FormCR0'].Begin0('0',1,25,25,'FormCR0');
	Repeater['RoofR1'].Begin0('0',1,25,25,'RoofR1');
	Repeater['RoofCR1'].Begin0('0',1,25,25,'RoofCR1');
//他
	Repeater['FormL0'].Begin0('101',1,25,25,'FormL0');
	Repeater['FormCL0'].Begin0('101', 1, 25,25,'FormCL0');
	Repeater['RoofL1'].Begin0('101', 1, 25,25,'RoofL1');
	Repeater['RoofCL1'].Begin0('101', 1, 25,25,'RoofCL1');

8550;
	Station['sta4'].Put(1,-5,5);
	Structure['Stop'].Put('0',-2,0,0,0,0,0,0,5);

8560;
//自
	Repeater['FormR0'].End();
	Repeater['FormCR0'].End();
	Repeater['RoofR1'].End();
	Repeater['RoofCR1'].End();
//他
	Repeater['FormL0'].End();
	Repeater['FormCL0'].End();
	Repeater['RoofL1'].End();
	Repeater['RoofCL1'].End();

//下溝
9790;
//自のみ
	Repeater['FormR0'].Begin0('0',1,25,25,'FormR0');
	Repeater['FormCR0'].Begin0('0',1,25,25,'FormCR0');
	Repeater['RoofR1'].Begin0('0',1,25,25,'RoofR1');
	Repeater['RoofCR1'].Begin0('0',1,25,25,'RoofCR1');

9880;
	Station['sta5'].Put(1,-5,5);
	Structure['Stop'].Put('0',-2,0,0,0,0,0,0,5);

9890;
//自のみ
	Repeater['FormR0'].End();
	Repeater['FormCR0'].End();
	Repeater['RoofR1'].End();
	Repeater['RoofCR1'].End();
	
//相武台下
12710;
//自
	Repeater['FormR0'].Begin0('0',1,25,25,'FormR0');
	Repeater['FormCR0'].Begin0('0',1,25,25,'FormCR0');
	Repeater['RoofR1'].Begin0('0',1,25,25,'RoofR1');
	Repeater['RoofCR1'].Begin0('0',1,25,25,'RoofCR1');
//他
	Repeater['FormL0'].Begin0('101',1,25,25,'FormL0');
	Repeater['FormCL0'].Begin0('101', 1, 25,25,'FormCL0');
	Repeater['RoofL1'].Begin0('101', 1, 25,25,'RoofL1');
	Repeater['RoofCL1'].Begin0('101', 1, 25,25,'RoofCL1');

12800;
	Station['sta6'].Put(1,-5,5);
	Structure['Stop'].Put('0',-2,0,0,0,0,0,0,5);

12810;
//自
	Repeater['FormR0'].End();
	Repeater['FormCR0'].End();
	Repeater['RoofR1'].End();
	Repeater['RoofCR1'].End();
12785;
	Repeater['FormL0'].End();
	Repeater['RoofL1'].End();
12770;
//他

	Repeater['FormCL0'].End();

	Repeater['RoofCL1'].End();

//入谷
14400;
//自のみ
	Repeater['FormL0'].Begin0(0,1,25,25,'FormL0');
	Repeater['FormCL0'].Begin0(0, 1, 25,25,'FormCL0');
	Repeater['RoofL1'].Begin0(0, 1, 25,25,'RoofL1');
	Repeater['RoofCL1'].Begin0(0, 1, 25,25,'RoofCL1');

14490;
	Station['sta7'].Put(1,-5,5);
	Structure['Stop'].Put('0',2,0,0,0,0,0,0,5);

14500;
//自
	Repeater['FormL0'].End();
	Repeater['FormCL0'].End();
	Repeater['RoofL1'].End();
	Repeater['RoofCL1'].End();

//海老名
17300;
//自
	Repeater['FormR0'].Begin0('0',1,25,25,'FormR0');
	Repeater['FormCR0'].Begin0('0',1,25,25,'FormCR0');
	Repeater['RoofR1'].Begin0('0',1,25,25,'RoofR1');
	Repeater['RoofCR1'].Begin0('0',1,25,25,'RoofCR1');
//他
	Repeater['FormL0'].Begin0('101',1,25,25,'FormL0');
	Repeater['FormCL0'].Begin0('101', 1, 25,25,'FormCL0');
	Repeater['RoofL1'].Begin0('101', 1, 25,25,'RoofL1');
	Repeater['RoofCL1'].Begin0('101', 1, 25,25,'RoofCL1');

17390;
	Station['sta8'].Put(1,-5,5);
	Structure['Stop'].Put('0',-2,0,0,0,0,0,0,5);

17400;
//自
	Repeater['FormR0'].End();
	Repeater['FormCR0'].End();
	Repeater['RoofR1'].End();
	Repeater['RoofCR1'].End();
//他
	Repeater['FormL0'].End();
	Repeater['FormCL0'].End();
	Repeater['RoofL1'].End();
	Repeater['RoofCL1'].End();


//ここから厚木
//19280;
//	Station['sta9'].Put(1,-5,5);
//	Structure['Stop'].Put('0',-2,0,0,0,0,0,0,5);
//21880;
//	Station['sta10'].Put(1,-5,5);
//23480;
//	Station['sta11'].Put(1,-5,5);
//24880;
//	Station['sta12'].Put(1,-5,5);
//26280;
//	Station['sta13'].Put(1,-5,5);
//28380;
//	Station['sta14'].Put(1,-5,5);
//30080;
//	Station['sta15'].Put(1,-5,5);
//32180;
//	Station['sta16'].Put(1,-5,5);
//33480;
//	Station['sta17'].Put(1,-5,5);

